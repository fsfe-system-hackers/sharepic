// SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

module git.fsfe.org/fsfe-system-hackers/sharepic/backend

go 1.22

require (
	github.com/oxzi/syscallset-go v0.1.6
	golang.org/x/sync v0.11.0
	gopkg.in/gographics/imagick.v3 v3.7.2
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/elastic/go-seccomp-bpf v1.5.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
